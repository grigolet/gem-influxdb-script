# gem-influxdb-script

Script for ep-dt-gem setup to write data into influxdb.
The script reads from three files:
* Picolog: with data saved every minute about the rack values
* Wavedump cesium: data from cesium analysis using wavedump, done every hour
* Wavedump iron: data from Fe source analysis using wavedump, done every minute

The script reads from the three files and sends the data into the epdtmon database using the python influxdb apis.

Before running the script create a file called `configs.ini` in the same folder of the scripts with the following values
```ini
[db]
host=url-database.cern.ch
port=8081
database=database-name
username=username
password=username
ssl=True
verify_ssl=False

[picolog]
path=/path/to/picolog/file.out
meaning_path=/path/to/picolog_meaning.xlsx
sleep_time=60 # in seconds
place=gif
setup=gem

[wavedump_cs]
path=/path/to/SummaryGEM_Online_CF4_70%%.txt
meaning_path=/path/to/summary_meaning.xlsx
sleep_time=60 # in seconds
place=gif
setup=gem

[wavedump_fe]
path=/path/to/SummaryGEM_Online_CF4_70%%.txt
meaning_path=/path/to/summary_meaning.xlsx
place=gif
setup=gem
sleep_time=60 # in seconds
```