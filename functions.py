from configparser import ConfigParser
import pandas as pd
import time
from influxdb import InfluxDBClient, DataFrameClient
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def read_configs(filepath='configs.ini'):
    # Read config file for database credentials
    configs = ConfigParser()
    configs.read('configs.ini')

    return configs

def get_db_configs(configs):
    """Get the configurations of for the influxdb client
    connection as a dict where the key are the name of the 
    args to be passed and the values are the actual values for the
    connection"""
    db_configs = configs['db']
    # Read the configs for the database connection
    host = db_configs['host']
    port = db_configs.getint('port')
    database = db_configs['database']
    username = db_configs['username']
    password = db_configs['password']
    ssl = db_configs.getboolean('ssl')
    verify_ssl = db_configs.getboolean('verify_ssl')

    return dict(host=host, port=port, database=database, username=username,
                password=password, ssl=ssl, verify_ssl=verify_ssl)


def values_to_db(client, values, measurement_name, tags, time=None):
    measurement = [{
        "measurement": measurement_name,
        "tags": tags,
        "fields": values,
        "time": time or pd.Timestamp.now()
    }]
    result = client.write_points(measurement)
    return result


def process_wavedump_file(spectrum='cs'):
    configs = read_configs()
    db_configs = get_db_configs(configs)
    if spectrum == 'cs':
        wavedump_configs = configs[f'wavedump_{spectrum}']
    else:
        wavedump_configs = configs[f'wavedump_{spectrum}']
    tags = dict(place=wavedump_configs['place'], setup=wavedump_configs['setup'], spectrum='cs')
    path = wavedump_configs['path']
    meaning_path = wavedump_configs['meaning_path']
    client = InfluxDBClient(**db_configs)

    def read_wavedump_line(path, meaning_path, index=-1):
        meaning_df = pd.read_excel(meaning_path)
        column_names = meaning_df['description'].values
        renamed_column_names = [f'zero_{ix}' if value=='zero' else value for ix, value in enumerate(column_names)]
        df = pd.read_csv(path, names=renamed_column_names, delim_whitespace=True, parse_dates=[['yy', 'mm', 'dd', 'hh', 'min', 'ss']], index_col=0)

        return df.iloc[index]

    
    while True:
        line = read_wavedump_line(path, meaning_path)
        # Check if the line is okay
        try:
            line = pd.to_numeric(line)
            values = line.to_dict()
            timestamp = pd.to_datetime(line.name, format="%Y %m %d %H %M %S")  - pd.TimeDelta(hours=2)
            result = values_to_db(client, values, 'wavedump', tags, time=timestamp)
            print(result)
        except Exception as e:
            print('Error in converting line or saving to db: ', line)
            print(e)

        time.sleep(int(wavedump_configs['sleep_time']))


def process_picolog_file():
    # Read picolog configs
    configs = read_configs()
    db_configs = get_db_configs(configs)
    picolog_configs = configs['picolog']
    tags = dict(place=picolog_configs['place'], setup=picolog_configs['setup'])
    path = picolog_configs['path']
    meaning_path = picolog_configs['meaning_path']
    # Instantiatea the database client
    client = InfluxDBClient(**db_configs)

    def read_picolog_line(path, meaning_path, index=-1):
        """Read the last line from the path and use the
        meaning_path to identify the name of the columns
        """
        # First read the name of the columns
        meaning_df = pd.read_excel(meaning_path, header=None)
        column_names = meaning_df.iloc[0].values
        # Rename duplicate zeros names lines to avoid conflicts
        renamed_column_names = [f'zero_{ix}' if value=='zero' else value for ix, value in enumerate(column_names)]

        df = pd.read_csv(path, names=renamed_column_names, delim_whitespace=True, parse_dates=[['yy', 'mm', 'dd', 'hh', 'min', 'ss']], index_col=0)
        return df.iloc[index]
    
    while True:
        line = read_picolog_line(path, meaning_path)
        # Check if the line is okay
        try:
            line = pd.to_numeric(line)
            values = line.to_dict()
            timestamp = pd.to_datetime(line.name, format="%Y %m %d %H %M %S") - pd.TimeDelta(hours=2)
            result = values_to_db(client, values, 'picolog', tags, time=timestamp)
            print(result)
        except Exception as e:
            print('Error in converting line or saving to db: ', line)
            print(e)

        time.sleep(int(picolog_configs['sleep_time']))
