from threading import Thread
from functions import *


# Start a thread that every minute checks the picolog files, parse it
# and send data into the database
picolog_thread = Thread(target=process_picolog_file)
picolog_thread.start()

wavedump_cs_thread = Thread(target=process_wavedump_file, args=('cs'))
wavedump_cs_thread.start()

wavedump_fe_thread = Thread(target=process_wavedump_file, args=('fs'))
wavedump_fe_thread.start()